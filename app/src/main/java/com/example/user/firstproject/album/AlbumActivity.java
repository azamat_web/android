package com.example.user.firstproject.album;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.user.firstproject.MainActivity;
import com.example.user.firstproject.R;
import com.example.user.firstproject.component.RetrofitBuilder;
import com.example.user.firstproject.component.ServerApi;
import com.example.user.firstproject.model.Album;
import java.util.Collections;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AlbumActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);
        getAlbums();
    }

    public void createButton(List<Album> albums) {

        LinearLayout linearLayout = new LinearLayout(AlbumActivity.this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setBaselineAligned(false);
        linearLayout.setPadding(10, 10, 10, 15);

        for (final Album album: albums) {
            Button button = new Button(AlbumActivity.this);
            button.setId(album.getId());
            button.setTextSize(10);
            button.setText(album.getTitle());
            button.setGravity(Gravity.CENTER);
            linearLayout.addView(button);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), GridLayoutActivity.class);
                    i.putExtra("album_id", album.getId());
                    startActivity(i);
                }
            });
        }
        int color = Color.parseColor("#2611C0");
        int txtColor = Color.parseColor("white");
        Button button = new Button(AlbumActivity.this);
        button.setTextSize(10);
        button.setText("Назад");
        button.setBackgroundColor(color);
        button.setTextColor(txtColor);
        button.setGravity(Gravity.CENTER);
        linearLayout.addView(button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBackAlbum(v);
            }
        });

        this.setContentView(linearLayout, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

    }

    public void getAlbums() {

        final Retrofit retrofit = RetrofitBuilder.build();
        ServerApi api = retrofit.create(ServerApi.class);
        Call<List<Album>> call = api.albums();

        call.enqueue(new Callback<List<Album>>() {

            @Override
            public void onResponse(Call<List<Album>> call, Response<List<Album>> response) {
                List<Album> albumList = response.body();
                Collections.shuffle(albumList);
                albumList = albumList.subList(0, 10);
                createButton(albumList);
            }

            @Override
            public void onFailure(Call<List<Album>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void goBackAlbum(View view) {
        Intent intent = new Intent(AlbumActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
