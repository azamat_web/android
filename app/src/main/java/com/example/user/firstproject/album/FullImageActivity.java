package com.example.user.firstproject.album;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.user.firstproject.R;
import com.example.user.firstproject.component.ImageAdapter;
import com.squareup.picasso.Picasso;

public class FullImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        String path = extras.getString("path"); //
        ImageView imageView = (ImageView) findViewById(R.id.full_image_view);

        Picasso.with(this)
                .load(path)
                .into(imageView);

    }

}
