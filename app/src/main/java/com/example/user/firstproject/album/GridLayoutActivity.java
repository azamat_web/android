package com.example.user.firstproject.album;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.example.user.firstproject.MainActivity;
import com.example.user.firstproject.R;
import com.example.user.firstproject.component.ImageAdapter;
import com.example.user.firstproject.component.RetrofitBuilder;
import com.example.user.firstproject.component.ServerApi;
import com.example.user.firstproject.model.Photo;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class GridLayoutActivity extends AppCompatActivity {

    GridView gridview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_layout);
        Intent intent = getIntent();
        int album_id = intent.getExtras().getInt("album_id");
        gridview = (GridView) findViewById(R.id.gridview);
        getPhotos(album_id);
    }

    public void getPhotos(final int album_id) {
        final Retrofit retrofit = RetrofitBuilder.build();

        ServerApi api = retrofit.create(ServerApi.class);
        Call<List<Photo>> call = api.photos();

        call.enqueue(new Callback<List<Photo>>() {
            @Override
            public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
                List<Photo> photoList = response.body();
                List<String> photos = new ArrayList<>();

                for (int i = 0; i < photoList.size(); i++) {
                    if (photoList.get(i).getAlbumId().equals(album_id)){
                        photos.add(photoList.get(i).getThumbnailUrl());
                    }
                }
                final ImageAdapter imageAdapter = new ImageAdapter(GridLayoutActivity.this, photos);
                gridview.setAdapter(imageAdapter);

                gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                        String path = imageAdapter.getItem(position);
                        // Sending image id to FullImageActivity
                        Intent i = new Intent(getApplicationContext(), FullImageActivity.class);
                        // passing array index
                        i.putExtra("path", path);
                        startActivity(i);
                    }
                });
            }

            @Override
            public void onFailure(Call<List<Photo>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void goBackGrid(View view) {
        Intent intent = new Intent(GridLayoutActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
