package com.example.user.firstproject.component;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.user.firstproject.R;
import com.example.user.firstproject.model.Comment;

import java.util.ArrayList;

public class CommentAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Comment> comments = new ArrayList<>();
    private ArrayList<Comment> comment = new ArrayList<>();
    private LayoutInflater LInflater;


    public CommentAdapter(Context context, ArrayList<Comment> comments) {
        this.context = context;
        this.comments = comments;
        this.LInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return comments.size();
    }

    @Override
    public Comment getItem(int position) {
        return comments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        View v = convertView;

        if ( v == null){
            holder = new ViewHolder();
            v = LInflater.inflate(R.layout.activity_comment_item, parent, false);
            holder.name = (TextView) v.findViewById(R.id.textView);
            holder.body = ((TextView) v.findViewById(R.id.textView2));
            v.setTag(holder);
        }

        holder = (ViewHolder) v.getTag();
        comment.add(getItem(position));

        holder.name.setText(comment.get(position).getName());
        holder.body.setText(comment.get(position).getBody());

        return v;

    }

    private static class ViewHolder {
        private TextView name;
        private TextView body;
    }
}


