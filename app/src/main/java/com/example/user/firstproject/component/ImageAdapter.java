package com.example.user.firstproject.component;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import com.example.user.firstproject.R;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    private List<String> photos = new ArrayList<>();

    public ImageAdapter(Context c, List<String> photos) {
        this.mContext = c;
        this.photos = photos;
    }

    public int getCount() {
        return photos.size();
    }

    @Override
    public String getItem(int position) {
        return photos.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;

        if (convertView == null) {
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(320, 320));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            imageView = (ImageView) convertView;
        }

        String url = getItem(position);
        Picasso.with(mContext)
                .load(url)
                .placeholder(R.drawable.ic_launcher_background)
                .fit()
                .centerCrop().into(imageView);
        return imageView;
    }
}
