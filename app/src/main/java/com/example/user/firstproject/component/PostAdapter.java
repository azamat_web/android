package com.example.user.firstproject.component;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.user.firstproject.R;
import com.example.user.firstproject.model.Comment;
import com.example.user.firstproject.model.Post;

import java.util.ArrayList;

public class PostAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Post> posts = new ArrayList<>();
    private ArrayList<Post> post = new ArrayList<>();
    private LayoutInflater LInflater;


    public PostAdapter(Context context, ArrayList<Post> posts) {
        this.context = context;
        this.posts = posts;
        this.LInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return posts.size();
    }

    @Override
    public Post getItem(int position) {
        return posts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        View v = convertView;

        /*
         * В том случае, если вид элемента не создан, производится его создание
         * с помощью ViewHolder и тегирование данного элемента конкретным holder объектом
         */
        if ( v == null){
            holder = new ViewHolder();
            v = LInflater.inflate(R.layout.activity_post_item, parent, false);
            holder.title = (TextView) v.findViewById(R.id.textPostView);
            holder.body = ((TextView) v.findViewById(R.id.textPostView2));
            v.setTag(holder);
        }

        /*
         * После того, как все элементы определены, производится соотнесение
         * внешнего вида, данных и конкретной позиции в ListView.
         * После чего из ArrayList забираются данные для элемента ListView и
         * передаются во внешний вид элемента
         */
        holder = (ViewHolder) v.getTag();
        post.add(getItem(position));

        holder.title.setText(post.get(position).getTitle());
        holder.body.setText(post.get(position).getBody());

        return v;

    }

    private static class ViewHolder {
        private TextView title;
        private TextView body;
    }
}


