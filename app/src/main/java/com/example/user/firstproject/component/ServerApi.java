package com.example.user.firstproject.component;

import com.example.user.firstproject.model.Album;
import com.example.user.firstproject.model.Comment;
import com.example.user.firstproject.model.Photo;
import com.example.user.firstproject.model.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ServerApi {

    String BASE_URL = "https://jsonplaceholder.typicode.com/";

    @GET("albums")
    Call<List<Album>> albums();

    @GET("photos")
    Call<List<Photo>> photos();

    @GET("posts")
    Call<List<Post>> posts();

    @GET("/posts/{id}/comments")
    Call<List<Comment>> comments(@Path("id") int id);

}
