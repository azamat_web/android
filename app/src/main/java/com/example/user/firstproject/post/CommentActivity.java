package com.example.user.firstproject.post;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;
import com.example.user.firstproject.R;
import com.example.user.firstproject.component.CommentAdapter;
import com.example.user.firstproject.component.RetrofitBuilder;
import com.example.user.firstproject.component.ServerApi;
import com.example.user.firstproject.model.Comment;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CommentActivity extends AppCompatActivity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        listView = (ListView)findViewById(R.id.listViewComment);
        getComments();
    }

    public void getComments() {
        final Retrofit retrofit = RetrofitBuilder.build();
        ServerApi api = retrofit.create(ServerApi.class);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        int postId = extras.getInt("postId"); //
        Call<List<Comment>> call = api.comments(postId);

         call.enqueue(new Callback<List<Comment>>() {

            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                List<Comment> commentList = response.body();
                ArrayList<Comment> comments = new ArrayList<>();

                for (int i=0; i < commentList.size(); i++)
                    comments.add(commentList.get(i));

                final CommentAdapter gridVIewAdapter = new CommentAdapter(CommentActivity.this, comments);
                listView.setAdapter(gridVIewAdapter);
            }

            @Override
            public void onFailure(Call<List<Comment>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
