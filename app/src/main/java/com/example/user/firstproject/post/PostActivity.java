package com.example.user.firstproject.post;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.user.firstproject.R;
import com.example.user.firstproject.component.PostAdapter;
import com.example.user.firstproject.component.RetrofitBuilder;
import com.example.user.firstproject.component.ServerApi;
import com.example.user.firstproject.model.Post;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PostActivity extends AppCompatActivity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        listView = (ListView)findViewById(R.id.listView);
        getPosts();
    }

    public void getPosts() {

        final Retrofit retrofit = RetrofitBuilder.build();
        ServerApi api = retrofit.create(ServerApi.class);
        Call<List<Post>> call = api.posts();

        call.enqueue(new Callback<List<Post>>() {

            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                List<Post> postList = response.body();
                ArrayList<Post> posts = new ArrayList<>();
                Collections.shuffle(postList);
                postList = postList.subList(0, 10);

               for (int i=0; i < postList.size(); i++)
                   posts.add(postList.get(i));

                final PostAdapter postAdapter = new PostAdapter(PostActivity.this, posts);
                listView.setAdapter(postAdapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        int postId = postAdapter.getItem(position).getId();
                        Intent intent = new Intent(PostActivity.this, CommentActivity.class);
                        intent.putExtra("postId", postId);
                        startActivity(intent);

                    }
                });
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
